package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ContactActivity extends AppCompatActivity {

    private ArrayList<Contact> contacts = new ArrayList<Contact>();
    String[] contacts_String = {"Michel 03 27 66 66 66","Jean 01 23 50 50 50"};
    private ListView contactList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        //------------------------------Binding xml-------------------------------------------
        contactList = (ListView) findViewById(R.id.contactList);

        //-----------------------Toast------------------------
        Intent intent = getIntent();
        Toast toast = Toast.makeText(getApplicationContext(), "Bonjour, " + intent.getStringExtra("name"), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP|Gravity.LEFT, 0, 30);
        toast.show();

        //-----------------------Contacts---------------------
        //Création
        Contact contact1 = new Contact("Michel", "03 27 66 66 11");
        Contact contact2 = new Contact("Michelle", "03 27 66 66 12");
        contacts.add(contact1);
        contacts.add(contact2);

        //Liaison a la liste
        ArrayAdapter adapter = new ArrayAdapter<String>(this,
                R.layout.activity_contact, contacts_String);
        //contactList.setAdapter(adapter);
    }
}
