package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView titre;
    private EditText champsSaisie;
    private Button bouton;
    private android.support.v7.widget.AppCompatImageView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //------------------------------Binding xml-------------------------------------------
        titre = (TextView) findViewById(R.id.titre);
        champsSaisie = (EditText) findViewById(R.id.champsSaisie);
        bouton = (Button) findViewById(R.id.bouton);
        logo = (android.support.v7.widget.AppCompatImageView) findViewById(R.id.logo);

        //--------------------------------BOUTON----------------------------------------------
        //Bloquage bouton
        bouton.setEnabled(false);
        //Réactivation si nom saisie
        champsSaisie.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                bouton.setEnabled(s.toString().length() != 0); //Vrai si au moins une lettre
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //On click
        bouton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Récupération nom
                String name = champsSaisie.getText().toString();
                //Lancement seconde activité
                Intent contactActivity = new Intent(MainActivity.this, ContactActivity.class);
                contactActivity.putExtra("name", name);
                startActivity(contactActivity);
            }
        });
    }
}
